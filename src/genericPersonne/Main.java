package genericPersonne;

import java.util.*;

public class Main {

	public static void main(String[] args) {
		
		Humain<Personne> humain = new Humain<Personne>();
		Humain<Etudiant> humain2 = new Humain<Etudiant>();
		Humain<Voiture> humain3 = new Humain<Voiture>();
		
		humain.setVar(new Personne("Elon","Musk"));
		humain2.setVar(new Etudiant("Steeve", "Jobs", "Master informatique"));
		humain3.setVar(new Voiture("Tesla"));
		
		List<Humain<?>> humans = new ArrayList<Humain<?>>();
		humans.add(humain);
		humans.add(humain2);
		humans.add(humain3);
		
		for(Humain<?> human: humans) {
			System.out.println(human);
		}
	}
	

}
