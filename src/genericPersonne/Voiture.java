package genericPersonne;

public class Voiture {

	private String marque;

	public Voiture(String marque) {
		super();
		this.marque = marque;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	@Override
	public String toString() {
		return "Voiture [marque=" + marque + "]";
	}
	
	
}
