package genericPersonne;

public class Etudiant extends Personne {
	
	private String niveau;
	
	public Etudiant(String firstname, String lastname, String niveau) {
		super(firstname, lastname);
		this.niveau = niveau;
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	@Override
	public String toString() {
		return "Etudiant [niveau=" + niveau + ", toString()=" + super.toString() + "]";
	}





	
	
	

}
