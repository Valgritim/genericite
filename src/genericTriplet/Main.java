package genericTriplet;

public class Main {

	public static void main(String[] args) {
		
		Triplet<String> triplet = new Triplet<String>("hello", "shiny", "World");
		Triplet<Integer> triplet2 = new Triplet<Integer>(1, 2, 3);
		Triplet<Double> triplet3 = new Triplet<Double>(1.1, 2.2, 3.3);
		
		triplet.affiche();
		triplet2.affiche();
		triplet3.affiche();
		
		TripletH<String,Integer,Double> tripleth = new TripletH<String,Integer,Double>("Hello",10,5.2);
		tripleth.affiche();

	}

}
