package genericTriplet;

public class Triplet <T>{
	private T premier;
	private T second;
	private T troisieme;
	
	public Triplet(T premier, T second, T troisieme) {
		super();
		this.premier = premier;
		this.second = second;
		this.troisieme = troisieme;
	}

	public T getPremier() {
		return premier;
	}

	public void setPremier(T premier) {
		this.premier = premier;
	}

	public T getSecond() {
		return second;
	}

	public void setSecond(T second) {
		this.second = second;
	}

	public T getTroisieme() {
		return troisieme;
	}

	public void setTroisieme(T troisieme) {
		this.troisieme = troisieme;
	}

	@Override
	public String toString() {
		return "Triplet [premier=" + premier + ", second=" + second + ", troisieme=" + troisieme + "]";
	}
	
	public void affiche() {
		System.out.println( "premier = " + premier + ", second= " + second + ",troisieme= " + troisieme);
		
	}
	
	
}
