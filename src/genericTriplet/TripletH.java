package genericTriplet;

public class TripletH <T1,T2,T3>{
	
	private T1 premier;
	private T2 second;
	private T3 troisieme;
	
	public TripletH(T1 premier, T2 second, T3 troisieme) {
		super();
		this.premier = premier;
		this.second = second;
		this.troisieme = troisieme;
	}

	public T1 getPremier() {
		return premier;
	}

	public void setPremier(T1 premier) {
		this.premier = premier;
	}

	public T2 getSecond() {
		return second;
	}

	public void setSecond(T2 second) {
		this.second = second;
	}

	public T3 getTroisieme() {
		return troisieme;
	}

	public void setTroisieme(T3 troisieme) {
		this.troisieme = troisieme;
	}
	
	public void affiche() {
		System.out.println( "premier = " + premier + ", second= " + second + ",troisieme= " + troisieme);
		
	}
	

}
